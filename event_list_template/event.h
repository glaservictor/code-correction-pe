/************************************************************
*************************************************************

File: event.h

Autore: Francesco Bronzino

Description: 

*************************************************************
************************************************************/


#ifndef EVENT_H
#define EVENT_H

#include "types.h"

extern bool event_debug;

typedef enum ev{E_ARR, E_DEP, E_END, E_QUEUED}event_t;

class Event{
private:
	event_id ev_id;
	station_id s_id;
	s_time *system_time;
	s_time tsched;
	s_time arr_time;
	s_time service_time;
	int priority; //in un secondo momento
	event_t ecode;
	Event *next;
public:
	Event();
	Event(event_t opcode, s_time tsched, s_time *system_time, event_id ev_id, station_id s_id, s_time arr_time, s_time service_time, int priority);
	Event(const Event &e);
	Event(s_time *system_time);
	~Event();
	
	void set_all(const Event &e);
	
	void set_tsched(s_time new_time);
	s_time get_tsched();
	
	void set_evid(event_id ev_id);
	event_id get_evid();
	
	void set_sid(station_id s_id);
	station_id get_sid();
	
	void set_arrtime();
	s_time get_arrtime();
	
	void set_sertime(s_time new_time);
	s_time get_sertime();
	
	void set_priority(int priority);
	int get_priority();
	
	void set_code(event_t ecode);
	event_t get_code();
	
	void clear();
	
	void set_next(Event *e);
	
	Event *get_next_ptr();
	
	void print_state_event(FILE *f);
	
	void read_event(FILE *f);
	
	void print_state(ostream& o,int tabs);
	
	void print_state(ofstream& o,int tabs);
	
	void print_full_state(ostream& o,int tabs);
	
	void print_full_state(ofstream& o,int tabs);
	
	friend ostream& operator<<(ostream& o,const Event &e);
	friend bool operator<(const Event &e1,const Event &e2);
	friend bool operator==(const Event &e1,const Event &e2);
	friend bool operator!=(const Event &e1,const Event &e2);
};

Event *scan_event(ifstream& in, s_time *system_time);

#endif

