/************************************************************
*************************************************************

File: event_list.h

Author: Francesco Bronzino

Description: 

*************************************************************
************************************************************/


#ifndef EVENT_LIST_H
#define EVENT_LIST_H

#include "event.h"
#include "types.h"

extern bool event_list_debug;

class Event;

class Event_list{
private:
	Event *list;
	
public:
	Event_list();
	~Event_list();
	
	int schedule(Event *e);
	
	Event *get_first();
	
	Event *remove(Event *e);
	
	Event *get_event(event_id id);
	
	void print_state(ostream& o,int tabs);
	
	void print_state(ofstream& o,int tabs);
	
	void print_full_state(ostream& o,int tabs);
	
	void print_full_state(ofstream& o,int tabs);
	
	friend ostream& operator<<(ostream& o,Event_list &e);
};

Event_list *scan_event_list(ifstream& in, s_time *system_time);

#endif
