/************************************************************
*************************************************************

File: types.h

Autore: Francesco Bronzino

Descrizione: 

*************************************************************
************************************************************/

#ifndef TYPES_H
#define TYPES_H

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

typedef double s_time;
typedef uint32_t event_id;
typedef int station_id;

static inline void ptbs(ostream& o, int n) {
	for(int i = 0; i<n; i++)
		o << "\t";
}

static inline void ptbs(ofstream& o, int n) {
	for(int i = 0; i<n; i++)
		o << "\t";
}

#endif
